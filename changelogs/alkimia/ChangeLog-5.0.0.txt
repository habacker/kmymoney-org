commit d5f02ccd4acc66da65b26de246a439dc906eb5db
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Tue Dec 1 13:08:19 2015 +0200

    Remove these files since the changelog will be provided in the release notes.

commit a9fda25cb17f6412fd2f3c16b907da32e05e0c6d
Author: David Faure <faure@kde.org>
Date:   Sat Aug 15 00:21:26 2015 +0200

    update cmake_min_req to match kdelibs4 and enable newer policies, so that cmake can move on at some point

commit 2c2bf53d5ca30d33605b801df60d57d2151326ad
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Sun Oct 26 22:05:34 2014 +0200

    Use implicit sharing in AlkValue.
    
    Before this the biggest cost of using an AlkValue object, implicitly
    a KMyMoneyMoney object was assignment, construction and destruction.
    
    By using implicit sharing combined with a shared zero value, copying
    on assignment and construction can be greatly reduced.
    
    Implicit sharing was easy to implement using QSharedDataPointer.
    
    Bumped the library version because API changes were necessary. The
    mpq_class &valueRef() const method was bad beacause it was returning
    a non-const reference from a const function. Now There is a const
    version which will not detach the shared data while the non-const
    version will detach from the shared data.
    
    REVIEW: 120815
    REVIEW: 121030

commit 420f921a6258ded473d9f0773a6bc2f04d35e8a9
Author: Christian Dávid <christian-david@web.de>
Date:   Thu Nov 6 10:30:32 2014 +0100

    Added .reviewboard file
    
    So it is possible to use rb tools.

commit 2f33d56b282f8a2d12cceda72e632751ca04c00c
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Aug 31 18:59:22 2014 +0200

    Fix build system to work with CMake < 2.8.12
    
    The minimum CMake version supported is now 2.8.7 and the usage
    of the keyword PUBLIC (introduced in 2.8.12) has been reverted
    to LINK_PUBLIC (which is available in CMake 2.8.7+)

commit 37cace69a8aba82b9b160ba787cf2e2861e99389
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Sat Jul 26 14:18:55 2014 +0300

    Make PkgConfig optional and fix the library export mechanism.

commit 9c4d1ec7ab383d5623dd33a6869507b966283a91
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Fri Jul 25 21:31:00 2014 +0300

    Cleanup build system.

commit c3c83268d86688078e05be382de6e20761829282
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Thu Jun 26 23:51:45 2014 +0300

    Add gitignore and some missing stuff to CMakeLists.txt.

commit 5de8c6aadd2c46fe1d75635417479739e6858132
Author: Christian Dávid <christian-david@web.de>
Date:   Fri Apr 18 15:49:52 2014 +0200

    Added a cmake package configuration file
    
    This replaces the Find....cmake file. A user should not notice any
    changes. But:
    
    - CMAKE_PREFIX_PATH is honored now
    - Users do not have to find GMP themselves (which was a bug)
    - CMake users are not affected by a bug in PKG_CHECK_MODULES anymore
      more info: http://www.cmake.org/Bug/view.php?id=8644
    
    REVIEW: 117630

commit 7c61198e545f8fc549c0255c5b6df07b77b64957
Author: Patrick Spendrin <ps_ml@gmx.de>
Date:   Sat May 4 23:23:46 2013 +0200

    fix building under msvc

commit 8982bdef054cc13735b3fb47f139a52188e3e5ac
Author: Pino Toscano <pino@kde.org>
Date:   Sun Feb 3 12:09:21 2013 +0100

    tests: convert most of QVERIFY(a == b) as QCOMPARE(a, b)
    
    this way, if the check fails both actual and expected values are printed;
    also reorder in few cases the arguments to have first actual and then expected value

commit e2d1540573e8680d147432617c48fba3cbce3ba8
Author: Pino Toscano <pino@kde.org>
Date:   Sun Feb 3 11:35:23 2013 +0100

    AlkValueTest: add QTest::toString for AlkValue
    
    this way a string representation can be used in comparison failures

commit 894e4c65a03e6dc122e6da5c89213001767c4984
Author: Pino Toscano <pino@kde.org>
Date:   Sun Feb 3 11:27:49 2013 +0100

    remove 'void' from parameter-less methods
    
    in C++ it is not needed (while it is in C)

commit a65f04b995e95a3eeb85ae8dd20482a02906b740
Author: Pino Toscano <pino@kde.org>
Date:   Sun Dec 30 11:25:57 2012 +0100

    cmake: do not include ${QDBUS_INCLUDE_DIRS}
    
    old variable which is set to nothing

commit abcbc3f1cf8b5b7d6d59be791b8fa07dc568263b
Author: Pino Toscano <pino@kde.org>
Date:   Sun Dec 30 11:17:00 2012 +0100

    ChangeLog/ReleaseLog: move to the top-level
    
    this is the usual place for these files

commit 56009a94c1d9c519974bcc7549c8ee963ca5c84e
Author: Pino Toscano <pino@kde.org>
Date:   Sun Dec 30 11:15:30 2012 +0100

    cmake: set project()

commit 503f87d9f0960a830c95e8c9c135762872994086
Author: Pino Toscano <pino@kde.org>
Date:   Sun Dec 30 11:14:51 2012 +0100

    cmake: remove redundant cmake_minimum_required
    
    the one done in the top-level CMakeLists.txt is enough

commit 101c8ffb615d203f4ebff4bcdc3f39b45d682290
Author: Pino Toscano <pino@kde.org>
Date:   Sun Dec 30 11:13:49 2012 +0100

    cmake: move the search for pkg-config, kde4, gmp to the top-level
    
    otherwise ctest does not see any unit test
