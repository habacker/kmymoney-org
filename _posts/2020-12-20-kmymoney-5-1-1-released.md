---
title: 'KMyMoney 5.1.1 released'
date: 2020-12-20 00:00:00
layout: post
---
<a href="https://download.kde.org/stable/kmymoney/5.1.1/src/kmymoney-5.1.1.tar.xz.mirrorlist">KMyMoney version 5.1.1</a> is now available.

<p>The KMyMoney development team is proud to present the next maintenance release of its open source Personal Finance Manager.</p>

<p>Here is the list of the included bugfixes grouped by severity:</p>
<h3>major</h3>
<p>
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=426406">426406</a> Minus sign is missing for negative numbers</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=427519">427519</a> Merge of Payees Deletes all Payees when an error occurs</li>
  </ul>
</p>
<h3>normal</h3>
<p>
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=405204">405204</a> New account details window is not sized correctly</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=419275">419275</a> QIF Im-/Exporter do not have default profile in dropdown after initial installation</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=420016">420016</a> Spelling mistakes in German "Ausgabe" categories</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=421915">421915</a> Can't reposition the currency symbol</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=423259">423259</a> Faulty icons display</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=423509">423509</a> kmymoney : decimal separator not supported anymore</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=423870">423870</a> Report 'Transactions by Category' converts all records to base currency even if this option is not selected</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=424098">424098</a> Closed Accounts not hidden on Home view</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=424188">424188</a> Account names only display partially on Home View</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=424302">424302</a> Dates in "All dates" reports title not displayed</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=424305">424305</a> Reload home page failed</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=424321">424321</a> "Find transaction" does not respect filter</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=424344">424344</a> Accounts combobox in export wizard</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=424424">424424</a> Budget plugin and date format</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=424511">424511</a> Loan will not calculate and does</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=424674">424674</a> Transaction report is missing splits in certain circumstances</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=424745">424745</a> The word "Principal" is mispelled as "Principle" in KMyMoney's categories</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=425280">425280</a> KMM adds an extra "/" to the institution url</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=425530">425530</a> KMyMoney Missing currency - Zambia Kwacha (ZMW)</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=425533">425533</a> Price information to old currencies not added when adding successor</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=425934">425934</a> Cannot rename new currency</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=425935">425935</a> No way of changing smallest cash/money unit size</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=426151">426151</a> Remove not implemented settings option "Insert transaction type into No. field for new transactions"</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=426217">426217</a> "New Institution" edits instead of creating institution</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=426243">426243</a> Chip-TAN widget is not completely visible</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=428164">428164</a> Adding new category from menu bar</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=428776">428776</a> Do not expose KCMs in application launchers</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=429237">429237</a> xea2kmt fails to parse gnucash templates</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=429436">429436</a> Calculator button not working for individual budget fields</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=429489">429489</a> Make Searching Categories case insensitive</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=429491">429491</a> Newly created institution is not displayed in institutions view</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=430178">430178</a> App freezes when toggling currency in investment performance report</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=430409">430409</a> Can't cancel Schedule Details dialog presented on startup</li>
  </ul>
</p>
<h3>minor</h3>
<p>
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=424378">424378</a> Values of date picker runs out of control for scheduled payments and transactions</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=428434">428434</a> Column not deleted on update of a forecast with different cycle duration</li>
  </ul>
</p>
<h3>wishlist</h3>
<p>
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=429229">429229</a> Import opening balance flag from gnucash templates</li>
  </ul>
</p>
<p>A complete description of all changes can be found in the <a href="/changelogs/ChangeLog-5.1.1.txt">changelog</a>.</p>

The KMyMoney Development Team
