---
title: '        KMyMoney 5.0.0 released
      '
date: 2018-02-06 00:00:00 
layout: post
---

<p><a href="https://download.kde.org/stable/kmymoney/5.0.0/src/kmymoney-5.0.0.tar.xz.mirrorlist">KMyMoney version 5.0.0</a> is now available.</p><p>Please take a look at the <a href="https://kmymoney.org/release-notes.php">release notes</a> before installing.</p><p>The KMyMoney Development Team</p>