---
title: '        KMyMoney 4.7.0 Release Notes
      '
date: 2014-10-01 00:00:00 
layout: post
---

<p>It's been over 3 years since the last feature release is out on the
          street. During that time, many new features were added and many bugs were fixed.
          The team has decided it's time to get on the
          path to another stable release.</p><p>KMyMoney 4.7.0 is now available for download. It is KMyMoney 4.8 Beta
          1, only suitable for advanced users willing to help us stabilize and
          iron out the upcoming stable version.</p><p>Your help is important. Please download it, use it, and help us find issues to make a great release.
          Keep in mind that this version will still bark your dog away. Make
          extensive backups before using it.</p><p>Our plan is to have a stable version ready in December. You can find the
          schedule <a href="http://techbase.kde.org/Projects/KMyMoney#Release_schedule">here</a>.</p><p>Some of the highlights since the latest feature version are:
          <ul>
            <li>Revamped CSV import plugin</li>
            <li>New Tags feature</li>
            <li>Oxygen is now the default icon set</li>
            <li>For OFX imports, application ID can be entered manually now</li>
            <li>Many fixes in the import and online banking modules</li>
            <li>Performance improvements</li>
            <li>Many fixes for multi-platform issues</li>
          </ul>
        </p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-4.7.0.txt">changelog</a>.</p><p>The KMyMoney Development Team</p>