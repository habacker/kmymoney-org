commit b814552332e48949d9ed303b7f7a1c51b57366f7
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Wed Jul 10 08:01:26 2019 +0200

    Bumped version to 5.0.5

commit 8aca5bbef774dff06b5b927fe9e991c13b3ba0f4
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Jul 7 12:30:00 2019 +0200

    Fix indentation of comments

commit a9a99469f5a403f4ad72c4c0b8c5e012571bb4f9
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Jul 7 11:05:59 2019 +0200

    Fix a crash during start on non KDE desktops
    
    Calling kmymoney->show() directly before the event loop is started using
    exec() causes a crash when running on some non KDE desktops (e.g. XFCE)
    with QWebEngine enabled. Postponing the call until the event loop is
    active solved the problem.
    
    BUG: 407902
    FIXED-IN: 5.0.5

commit 7c0d6f85a55ece30eac875ca12904d0af0350ff1
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Jul 6 17:23:14 2019 +0200

    Fix spelling errors in comments
    
    No change in translatable strings

commit 29dad430ae1879b05185641947040df23bd262ac
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Jul 6 15:57:42 2019 +0200

    Use https for connections to ofxhome.com
    
    Using a secure protocol ensures that the data we receive from the
    directory server at www.ofxhome.com cannot be modified by a mitm
    attacker on the way to us as easy as with plain http.

commit 7cb2ee89756fdca9edab99b528f702c293781cf9
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Jul 6 15:36:18 2019 +0200

    Switch protocol to https
    
    Use secure protocol when connecting to the OFX test server

commit 00bf04711d001bf6885de8fda7d99b4058d1c964
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Jul 2 20:23:04 2019 +0200

    Show tags in ledger if payee is empty
    
    BUG: 352029
    FIXED-IN: 5.0.5

commit faf1edd57744e12864848aeeac867d12a0609c57
Author: Joshua Christopher <chrisjoshtopher@gmail.com>
Date:   Sat Jun 29 12:52:42 2019 -0400

    Updated screenshots from payee views for 5.0.5.
    
    Summary: Updated screenshots from payee views for 5.0.5 due to the change in a tab's name from "Default Account" to "Default Category"
    
    Reviewers: ostroffjh, tbaumgart
    
    Reviewed By: ostroffjh, tbaumgart
    
    Subscribers: tbaumgart, ostroffjh, #kmymoney
    
    Differential Revision: https://phabricator.kde.org/D22152

commit ab628d659ebcbea9265b62e8dc419c3bb20bbcb7
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Jun 29 14:16:29 2019 +0200

    Use mapped account only if information is present
    
    Using the OFX (file) importer may not provide information about the
    account the imported data relates to. In this case, it does not make
    sense to search for an online mapping. Doing so leads to false results,
    as without values for account or routing number the first mapped account
    will be selected which is wrong.
    
    This change does not try the mapping if both values are empty and allows
    the user to select the account as part of the process.
    
    CCBUG: 405206

commit da8afb4010d4fb3900819c084dea7e977510f2af
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu Jun 27 11:42:45 2019 +0200

    Allow to set version suffix through cmake cli arg
    
    In a git based development environment the version suffix is generated
    based on the git sha1. In a non git environment one can now override the
    version suffix with -DVERSION_SUFFIX=xxx as cmake argument.

commit 21ba9b3a747d1d3c93cff11d8e30f667d59f4d5c
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Jun 25 21:13:51 2019 +0200

    Provide more descriptive text for holiday region
    
    The holiday region selection showed the same text for different entries
    in the same country/language region. For Germany alone there were 14
    entries with the same text so one could not differentiate between them.
    
    This change fills the combo box with the description of the holiday
    region which allows to differentiate between those entries if it is
    available. If it is empty, the current more general text is used.

commit af55a52c4788e035eba9f309126d339b148e4898
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Jun 25 21:09:05 2019 +0200

    Improve tooltip for a setting
    
    GUI:

commit 02e5037185496d070bc1ebbcdc3034f9a1cded77
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Jun 25 21:07:52 2019 +0200

    Treat special days as processing days if they are workdays
    
    The holiday list contains entries that shows special events that are
    working days. Nevertheless, KMyMoney treated them as non processing
    days.
    
    This change makes sure to treat those entries as processing days.
    
    BUG: 407800
    FIXED-IN: 5.0.5

commit 972af5c3619a4c92b50003e4b83fbbc21a480d55
Merge: b642033ee d03732001
Author: Jack Ostroff <ostroffjh@users.sourceforge.net>
Date:   Tue Jun 25 12:16:32 2019 -0400

    Merge branch '5.0' of git.kde.org:kmymoney into 5.0

commit d037320011bad6b496e5f933f9a3741171ce8751
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Jun 25 13:36:57 2019 +0200

    Simplified testcase
    
    The non-breaking-space as input caused trouble on windows systems. Using
    a regular blank (0x20) works on both *nix and windows environments.

commit 0c7270efcc62d644caba0bfb78aa87387e6ec452
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Jun 25 13:02:43 2019 +0200

    Try to fix testcase on windows

commit 1bae453a73a19beb51c635173a7edd3681e632c4
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Jun 25 11:52:39 2019 +0200

    Verify that locale is correctly loaded during tests

commit b642033eef138bacb68405321c31a0ddd979a7c3
Author: Jack Ostroff <ostroffjh@users.sourceforge.net>
Date:   Mon Jun 24 16:44:19 2019 -0400

    Update manual to reflect payee view tab change "Default Account" to
    "Default Category"
    
    BUG: 409089
    FIXED-IN: 5.0.5

commit 7e641a2163fb83f44209d127319dae856bbec8b6
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Jun 24 18:31:28 2019 +0200

    Fix tab title of auto categorization tab in payees view
    
    The tab title "Default Account" was misleading when on the tab we use
    the term "Default Category". Also removed a superfluous newline in a
    string on that tab.
    
    BUG: 409089
    FIXED-IN: 5.0.5
    GUI:

commit d2dc4d7b5a9090b2a7c55b244644fa565478dc75
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Jun 24 18:27:35 2019 +0200

    Fix GnuCash importer to import currencies
    
    Support currencies that are stored with
      <cmdty:space>CURRENCY</cmdty:space>
    in addition to ones stored with
      <cmdty:space>ISO4217</cmdty:space>.
    
    Also, only load those currencies that are used in the GnuCash file.
    
    BUG: 409098
    FIXED-IN: 5.0.5

commit 8d4d60649fa0b1f21d64f1af89c481b7d209c7b7
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Jun 24 18:22:02 2019 +0200

    Keep attribute order in XML file for debug version
    
    Keep the DOM attributes in the same order each time a file is saved.
    Therefore, don't use QHash for security relevant things from here on.
    See
    http://qt-project.org/doc/qt-5.0/qtcore/qhash.html#algorithmic-complexity-attacks
    for details.
    
    This is only active if build with DebugFull or DebugKMM and not used in
    production code.

commit 85eac02328369154080f0b739c46418a92ebe867
Author: l10n daemon script <scripty@kde.org>
Date:   Sun Jun 23 17:23:57 2019 +0200

    GIT_SILENT made messages (after extraction)

commit 686945965a6ffadf9460353f4df46b7f933591b4
Author: Jeremy Whiting <jpwhiting@kde.org>
Date:   Sat Jun 15 21:00:09 2019 -0600

    Add option to hide liability accounts with zero balance from home.
    
    In order to make the liabilities summary on the home screen easier
    to read (and fit on one screen if there are many) a new option to
    hide liability accounts that have zero balance has been added here.
    It seems to work well, but any comments/review are welcome.

commit ce17b1eeab73af593a0c09805801fbb413d71763
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Jun 17 10:58:39 2019 +0200

    Improve global storage of standard precision
    
    (cherry picked from commit 2bbdc29bc9c77a14eaf4e560ad855db1d422c3ee)

commit 0ba9f0550f7c54ff7a70e07c3a08c53b076319d9
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Jun 17 10:57:50 2019 +0200

    Reimplemented AmountValidator
    
    (cherry picked from commit c3442d0714964b70a04da21a289bdb8806f1617b)

commit a96eabe7f17409e45da1db0ff0d7c0f6e98b5573
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Jun 17 10:57:00 2019 +0200

    Added testcases for AmountValidator
    
    (cherry picked from commit 0d17b5b7ceec80ea005712f87f415768eb7d7f31)

commit 7949caa8901846e01b294e1265c64aa6c4775ec5
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Jun 15 11:54:27 2019 +0200

    doc: Write docbook date in standard format
    
    (cherry picked from commit 885d68311f938a11af0a7ec80f2084198c6175fe)

commit 8e49874f0a7b4058470fa3375a5b0e661465cd08
Author: l10n daemon script <scripty@kde.org>
Date:   Tue Jun 11 08:44:35 2019 +0200

    SVN_SILENT made messages (.desktop file) - always resolve ours
    
    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit 7b0c97ed6df1564bdd1f53aa7d99d144195652ff
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Jun 10 11:53:09 2019 +0200

    Use mapped account only if information is present
    
    Using the AqBanking file importer may not provide information about the
    account the imported data relates to. In this case, it does not make
    sense to search for an online mapping. Doing so leads to false results,
    as without values for account or routing number the first mapped account
    will be selected which is wrong.
    
    This change does not try the mapping if both values are empty and allows
    the user to select the account as part of the process.
    
    BUG: 408494
    FIXED-IN: 5.0.5

commit fc01f927393d5a5b037529fd4342c9213dac603f
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Wed Jun 5 18:55:45 2019 +0200

    Fix multi-month budget report
    
    The aggregation of budgets for multiple months (e.g. for quarterly
    reports) did not work correctly.
    
    This change fixes the problem.
    
    BUG: 393752
    FIXED-IN: 5.0.5

commit efc91150ccfb776ea1a2e131ba528d2079bfb85b
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Tue Jun 4 01:07:56 2019 +0200

    maintainer/release-windows-packages: fix ticket description for src packages

commit 29342ee65bec26a9d58fb84eba237184df292da6
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Tue Jun 4 01:01:47 2019 +0200

    maintainer/release-windows-packages: apply fixes to release kmymoney 5.x

commit 34d12e8fdc4e8a7c18f18c5aa665bebfb3323a9d
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Jun 2 11:56:07 2019 +0200

    Update payee in matched transaction
    
    In case two payees are merged and one of them is referenced in a matched
    transaction, it could happen that the transaction was removed from the
    users data leading to incorrect data.
    
    This change also updates the payee identifier in the matched transaction
    should it exist and thus avoids data loss.
    
    Deleting a payee that is referenced in a matched transaction still is
    not possible but does not result in data loss.
    
    BUG: 408205
    FIXED-IN: 5.0.5

commit 09fc41b4b872e0a5ec4ce2b955f99c04ae975d91
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue May 28 13:52:00 2019 +0200

    Reset column assignment when out of bounds
    
    In case the column assignment is cleared using the clear all button, the
    profile is not updated because there is no change in index in combobox
    widget and the connected slot for profile update will never be called.
    
    This change makes sure that in such a case (previous assignment is out
    of bounds for the current file) the assignment is reset before it is
    used. This avoids the out-of-bounds crash later on.
    
    BUG: 408026
    FIXED-IN: 5.0.5

commit ea3d48b31d5914e1584141342e9b1c44fa8efb4f
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon May 27 15:53:53 2019 +0200

    Set precision before value
    
    To work properly, the precision should be set before a value is loaded
    into the widget

commit bdefe7eeedc29316d16f55615f7736c797ef844e
Author: l10n daemon script <scripty@kde.org>
Date:   Sat May 25 08:43:08 2019 +0200

    SVN_SILENT made messages (.desktop file) - always resolve ours
    
    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit 023993800f9a66ceb52f7399bf28a4dbca0ef607
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Fri May 24 21:37:31 2019 +0200

    update screenshots
    
    (cherry picked from commit 7611781f32a3040ac434dc1fe1e82619a58bff9f)

commit 878149473b9489dc81f6a7fdabe952a941907298
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun May 19 11:12:53 2019 +0200

    Improve consistency warning message
    
    The term 'investment' in this message is misleading. 'Security' is a
    better choice.
    
    GUI:

commit d729796b4751b8697e84b790c3f05de549608764
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Fri Mar 1 10:20:05 2019 +0100

    Allow to include transfers in querytable reports
    
    Summary:
    This change adds a switch to report configuration which allows to treat
    transfers as income or expense in transaction reports (e.g. Transactions
    by payee, etc.)
    
    {F6820138}
    
    Reviewers: ostroffjh
    
    Differential Revision: https://phabricator.kde.org/D21150

commit ab6c7e2c722eba1a7d9382a5229a6bacde1110ea
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu May 16 09:43:04 2019 +0200

    Always unload model before a new load
    
    Since the count is known during the load we can also preserve some space
    to improve performance a bit.

commit 5824844052b429c1ef6b7f62b8163aaa83add09f
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat May 11 16:03:50 2019 +0200

    Allow to edit loan account not assigned to institution
    
    BUG: 402672
    FIXED-IN: 5.0.5

commit c9225875b9839947b6c98ecea82132cb064b70e1
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat May 11 12:29:40 2019 +0200

    Don't show false online statement balance
    
    In case there is no online statement balance available in the statement
    import, don't show false information in the ledger
    
    BUG: 407422
    FIXED-IN: 5.0.5

commit 89f10a501fcc606c10aa0080cf8633d0eeaae1a7
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat May 11 12:17:40 2019 +0200

    Remove trailing line endings in CSV import
    
    In case more than one column is assigned to the memo import and the
    trailing cells are empty, unnecessary lines are imported. This change
    removes those empty lines at the end of the memo field.

commit f9a1d6eec37eb24f0725dadf30d46d159473e282
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu May 9 22:17:01 2019 +0200

    Fix storage of schedules
    
    Fix code to provide prior behavior
    
    BUG: 406082
    FIXED-IN: 5.0.4

commit 6bffb7d0d9b68ad97130ed167d6174efd43811e5
Author: Pino Toscano <pino@kde.org>
Date:   Mon Apr 8 20:57:57 2019 +0200

    doc: drop almost all bits related to installation
    
    Summary:
    In the majority of the cases, users install kmymoney as available in
    their distribution, and thus most of this content is not useful; users
    that want to compile kmymoney from sources find already instructions on
    kmymoney.org, in the development wikis, and in the in-source files, so
    no need to duplicate them in a user-taylored documentation.
    
    Furthermore, the current building instructions are outdated, and an
    updated version, together with any FAQ related to building from sources
    and developing on kmymoney, belongs to online resources (easier to keep
    up-to-date).
    
    Test Plan:
    - build the documentation
    - verify the mentioned bits are not present
    
    Reviewers: ostroffjh
    
    Reviewed By: ostroffjh
    
    Subscribers: tbaumgart, #kmymoney, kde-doc-english
    
    Tags: #documentation
    
    Differential Revision: https://phabricator.kde.org/D20389

commit 89e8db9e7d55c5268afde436497de147e7d65fb3
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon May 6 17:58:39 2019 +0200

    Respect the "view/show all accounts" option in all views
    
    Respect the option also in the accounts, categories and institution view
    
    BUG: 407021
    FIXED-IN: 5.0.5

commit 20187306a2382b8e7f48acb5fc4112f17f77d0ab
Author: l10n daemon script <scripty@kde.org>
Date:   Mon May 6 07:46:04 2019 +0200

    GIT_SILENT made messages (after extraction)
