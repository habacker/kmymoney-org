---
title: The KMyMoney user manuals
layout: page
---

The user manuals are hosted by [docs.kde.org](https://docs.kde.org).

[KMyMoney (en)](https://docs.kde.org/?application=kmymoney&branch=stable5&language=en) [ (pdf)](https://docs.kde.org/?application=kmymoney&branch=stable5&language=en&path=kmymoney.pdf)

[KMyMoney (de)](https://docs.kde.org/?application=kmymoney&branch=stable5&language=de) [ (pdf)](https://docs.kde.org/?application=kmymoney&branch=stable5&language=de&path=kmymoney.pdf)

[KMyMoney (es) (outdated)](https://docs.kde.org/?application=kmymoney&branch=stable4&language=es)

<!-- https://docs.kde.org/?application=kmymoney&branch=stable4&language=es&path=kmymoney.pdf"> (pdf) -->

[KMyMoney (et) (outdated)](https://docs.kde.org/?application=kmymoney&branch=stable4&language=et)
<!-- https://docs.kde.org/?application=kmymoney&branch=stable4&language=et&path=kmymoney.pdf"> (pdf) -->

[KMyMoney (fr)](https://docs.kde.org/?application=kmymoney&branch=stable4&language=fr)
[ (pdf)  (outdated)](https://docs.kde.org/?application=kmymoney&branch=stable4&language=fr&path=kmymoney.pdf)

[KMyMoney (it)](https://docs.kde.org/?application=kmymoney&branch=stable5&language=it)
[ (pdf)](https://docs.kde.org/?application=kmymoney&branch=stable5&language=it&path=kmymoney.pdf)

[KMyMoney (nl)](https://docs.kde.org/?application=kmymoney&branch=stable5&language=nl)
[ (pdf)](https://docs.kde.org/?application=kmymoney&branch=stable5&language=nl&path=kmymoney.pdf)

[KMyMoney (pt)](https://docs.kde.org/?application=kmymoney&branch=stable4&language=pt)
[ (pdf)   (outdated)](https://docs.kde.org/?application=kmymoney&branch=stable4&language=pt&path=kmymoney.pdf)

[KMyMoney (pt_BR)](https://docs.kde.org/?application=kmymoney&branch=stable5&language=pt_BR)
[ (pdf)](https://docs.kde.org/?application=kmymoney&branch=stable5&language=pt_BR&path=kmymoney.pdf)

[KMyMoney (sv)](https://docs.kde.org/?application=kmymoney&branch=stable5&language=sv)
[ (pdf)](https://docs.kde.org/?application=kmymoney&branch=stable5&language=sv&path=kmymoney.pdf)

[KMyMoney (uk)](https://docs.kde.org/?application=kmymoney&branch=stable5&language=uk)
[ (pdf)](https://docs.kde.org/?application=kmymoney&branch=stable5&language=uk&path=kmymoney.pdf)

