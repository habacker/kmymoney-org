---
title: '        KMyMoney 4.6.2 Release Notes
      '
date: 2012-02-04 00:00:00 
layout: post
---

<p>The KMyMoney Team is pleased to announce the immediate availability of KMyMoney version
          4.6.2. This version contains quite a few fixes for bugs found in <a href="release-notes.php#itemKMyMoney461ReleaseNotes">4.6.1</a> since it was
          released three months ago.</p><p>Here is a list of the most important changes since the last stable release:
          <ul>
            <li>OFX files with UTF-8 data can now be imported correctly <a href="https://bugs.kde.org/show_bug.cgi?id=291685">#291685</a></li>
            <li>Fix displaying the 'Enter/Skip schedule' action icons in the homepage on Windows <a href="https://bugs.kde.org/show_bug.cgi?id=291480">#291480</a></li>
            <li>Fixed the initial size of the schedule entry dialog in some use cases <a href="https://bugs.kde.org/show_bug.cgi?id=236475">#236475</a></li>
            <li>Fixed a hang in reports in some scenarios <a href="https://bugs.kde.org/show_bug.cgi?id=290487">#290487</a></li>
            <li>Fixed some Finance::Quote related problems on Windows</li>
            <li>Allow editing the memo of multiple transactions <a href="https://bugs.kde.org/show_bug.cgi?id=289351">#289351</a></li>
            <li>Fix schedule handling <a href="https://bugs.kde.org/show_bug.cgi?id=290059">#290059</a></li>
            <li>Make the QIF import on Windows usable <a href="https://bugs.kde.org/show_bug.cgi?id=290483">#290483</a></li>
            <li>Fix GnuCash file import <a href="https://bugs.kde.org/show_bug.cgi?id=283848">#283848</a></li>
            <li>Improve item navigation using the keyboard <a href="https://bugs.kde.org/show_bug.cgi?id=283804">#283804</a></li>
            <li>Scheduled transactions can now be correctly skipped <a href="https://bugs.kde.org/show_bug.cgi?id=288647">#288647</a> or ignored <a href="https://bugs.kde.org/show_bug.cgi?id=254363">#254363</a> when automatic entry is enabled</li>
            <li>Fix the budgets that somehow still reference invalid accounts <a href="https://bugs.kde.org/show_bug.cgi?id=288279">#288279</a></li>
          </ul>
        </p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-4.6.2.txt">changelog</a>. We highly recommend upgrading to 4.6.2 as soon as possible. We would also recommend packagers to endorse this version as their latest stable version of KMyMoney due to the number of fixes available vs. <a href="release-notes.php#itemKMyMoney453ReleaseNotes">4.5.3</a>. During this bugfix cycle a new release of <a href="https://projects.kde.org/projects/extragear/office/alkimia">libalkimia</a> was also <a href="http://opendesktop.org/content/show.php?content=137323">made available</a>.</p><p>The KMyMoney Development Team</p>