<?php

# setup session
$lifetime = 360;
ini_set('session.use_strict_mode', 1);
echo session_start(['cookie_lifetime' => $lifetime,]);

# requires enough allowed memory for php
$useCurl = false;

function cleanup($signal=0) {
    static $done = false;
    global $tmpdir,$debug;

    if (true || $done || $debug)
        return 0;
    $cmd="rm -rf $tmpdir";
    exec($cmd, $output, $result);
    mylog("$cmd -> $result");
    return $result;
}

function createTempDir(int $mode = 0700): string
{
    do {
        $tmp = sys_get_temp_dir().'/unpack-'.mt_rand();
    } while (!@mkdir($tmp, $mode));
    return $tmp;
}

function mylog($s)
{
    global $debug;
    if ($debug) {
        echo "<pre>$s</pre>";
        ob_flush();
        flush();
    } else
        error_log($s);
}

$debug = isset($_REQUEST['debug']) && $_REQUEST['debug'] == 1;
$validRequest = isset($_REQUEST['key']) && isset($_SESSION[$_REQUEST['key']]);

if ($debug || !$validRequest) {
    header("Content-Type: text/html");
    echo "<!DOCTYPE html><html><body>";
}

if (!$validRequest) {
    echo "<pre>Unfortunately this request cannot be processed</pre>";
    echo "</body></html>";
    exit(0);
}

if ($debug) {
    echo "<pre>".session_id()." ".session_save_path()."</pre>";
    echo "<pre>".print_r($_SESSION, TRUE)."</pre>";
}

$key = $_REQUEST['key'];
$url = $_SESSION[$key];

# install signal handler for cleanup on kill
pcntl_async_signals(true);
pcntl_signal(SIGINT, 'cleanup');
pcntl_signal(SIGTERM, 'cleanup');

# create temporary dir
$tmpdir = createTempDir();
mylog("created temporary dir '$tmpdir'");

# fetch file
$array=explode("/",$url);
$out = "$tmpdir/".end($array);
mylog("out=$out");
if ($useCurl && function_exists('curl_init')) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FILE, $out); 
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);
    curl_close($ch);
    mylog("fetching $url to '$out' -> $result");
} else {
    $cmd = "cd $tmpdir; wget -q '$url'";
    unset($output);
    exec($cmd, $output, $result);
    mylog("$cmd -> $result");
    if ($result) {
        echo "$cmd -> $result";
        cleanup();
        exit(3);
    }
}

# init rpm database
$cmd = "rpmdb --root=$tmpdir --initdb";
unset($output);
exec($cmd, $output, $result);
mylog("$cmd -> $result");
if ($result) {
    echo "$cmd -> $result";
    cleanup();
    exit(2);
}

# install file
$cmd = "cd $tmpdir; rpm --root=$tmpdir -i '$out'";
unset($output);
exec($cmd, $output, $result);
mylog("$cmd -> $result");
if ($result) {
    echo "$cmd -> $result";
    print_r($output);
    cleanup();
    exit(4);
}

# get unpackaged filename
$cmd = "cd $tmpdir; find -name '*.7z' | sed 's,./,,g'";
unset($output);
exec($cmd, $output, $result);
mylog("$cmd -> $result");
if ($result) {
    echo "$cmd -> $result";
    print_r($output);
    cleanup();
    exit(5);
}
$fileName = $output[0];
$filePath = "$tmpdir/$fileName";
mylog("downloadfile=$filePath");

if ($debug) {
    mylog("<pre>file size='".filesize("$filePath")."'</pre>");
    echo "</body></html>";
    cleanup();
    exit(0);
}

# stream file
header("Content-Type: application/x-7z-compressed");
header("Content-Length: ".filesize("$filePath"));
header('Content-Disposition: attachment; filename="' . $fileName . '"');
header('Content-Transfer-Encoding: binary');
// Send Headers: Prevent Caching of File
header('Cache-Control: private');
header('Pragma: private');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

readfile("$filePath");
cleanup();
exit(0);
