---
layout: page
title: Download
sources:
  - name: Linux
    icon: /assets/img/tux.png
    description: >
        KMyMoney is readily available on the majority of Linux distributions. You
        can install it using the Software Center provided by your vendor.


        Alternatively, for those who want to use a version newer than the one available
        in their Software Center, we provide <a href="/appimage.html">AppImage packages</a>.
  - name: Windows
    icon: /assets/img/windows.svg
    description: >
        An installation package for an older stable release can be found on our
        [download server](https://download.kde.org/stable/kmymoney/5.0.6/).


        We also provide daily generated preview builds from our [stable](https://binary-factory.kde.org/job/KMyMoney_Release_win64/) and [master](https://binary-factory.kde.org/job/KMyMoney_Nightly_win64/) branches.
  - name: macOS
    icon: /assets/img/macOS.svg
    description: >
        Daily built DMG package for [stable version](https://binary-factory.kde.org/view/MacOS/job/KMyMoney_Release_macos/lastStableBuild/)
        is available for macOS.


        An experimental Homebrew package is provided by [Homebrew KDE project](https://invent.kde.org/packaging/homebrew-kde).


        Lastly, the legacy KMyMoney4 builds are also available in [MacPorts](https://www.macports.org/ports.php?by=name&substr=kmymoney).
  - name: Source code archive
    icon: /assets/img/ark.svg
    description: >
        You can find the source code of the KMyMoney latest stable release
        [here](https://download.kde.org/stable/kmymoney/) and the installation
        instructions in the [KDE TechBase wiki](https://techbase.kde.org/Projects/KMyMoney#Installation)
  - name: Git
    icon: /assets/img/git.svg
    description: >
        The KMyMoney Git repository can be found in the [KDE GitLab](https://invent.kde.org/office/kmymoney).


        To clone KMyMoney use:<br>
        <code>git clone https://invent.kde.org/office/kmymoney.git</code>
---

<h1>Download</h1>

<table class="distribution-table">
{% for source in page.sources %}
    <tr class="title-row">
        <td rowspan="2" width="100">
            <img src="{{ source.icon }}" alt="{{ source.name }}">
        </td>
        <th>{{ source.name }}</th>
    </tr>
    <tr>
        <td>{{ source.description | markdownify }}</td>
    </tr>
{% endfor %}
</table>
