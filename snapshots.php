<?php

# setup session
$lifetime = 360;
ini_set('session.use_strict_mode', 1);
session_start(['cookie_lifetime' => $lifetime,]);

# reload page to avoid stale links which are stored in the session
header("Refresh:$lifetime");

$verbose=0;
$product = "KMyMoney";
$obsPackageRoot="https://build.opensuse.org/package/show/";
$pathRoot="home:rhabacker:branches:windows:mingw";
$urlRoot="https://download.opensuse.org/repositories/";
$repoRoot="openSUSE_Leap_15.1";
$products = array(
    array(
        "name" => "KMyMoney 5.1 (stable) (Aqbanking6, Qt5.11, KF5.65)",
        "path" => array(
            "32" => "windows:mingw:win32",
            "64" => "windows:mingw:win64",
        ),
        "project" => ":snapshots",
        "package" => array(
            "name" => "kmymoney5",
            "version" => "5.1",
        ),
        "giturl" => "https://invent.kde.org/office/kmymoney/-/commits/5.1/",
        "anchor" => "kmymoney5-aq6",
    ),
    array(
        "name" => "KMyMoney 5.1 (stable) (Aqbanking6, Qt5.9, KF5.47)",
        "project" => ":kmymoney5",
        "package" => array(
            "name" => "kmymoney5",
            "version" => "5.1",
        ),
        "giturl" => "https://invent.kde.org/office/kmymoney/-/commits/5.1/",
        "anchor" => "kmymoney5-aq6-kf547",
    ),
    array(
        "name" => "KMyMoney4 (stable) (Aqbanking6, KDE4)",
        "project" => ":kmymoney",
        "package" => "kmymoney",
        "giturl" => "https://invent.kde.org/office/kmymoney/-/commits/4.8/",
        "anchor" => "kmymoney4-aq6",
    ),
    array(
        "name" => "KMyMoney4 (stable) (Aqbanking5, KDE4)",
        "project" => ":kmymoney-aq5",
        "package" => "kmymoney",
        "giturl" => "https://invent.kde.org/office/kmymoney/-/commits/4.8/",
        "anchor" => "kmymoney4-aq5",
    ),
    array(
        "name" => "KMyMoney4 (staging branch) (Aqbanking5, KDE4)",
        "project" => ":staging",
        "package" => "kmymoney",
        "giturl" => "https://github.com/rhabacker/kmymoney/commits/4.8-staging",
        "anchor" => "kmymoney4-aq5-staging",
    ),
);

$archs = array( "32", "64");

function hasRpm()
{
    static $check = -1;
    if ($check == -1)
        $check = is_executable(trim(shell_exec("which rpm 2>/dev/null")));
    return $check;
}

function getOBSPackageName($product)
{
    $package = is_array($product['package']) ? (isset($product['package']['obsname']) ? $product['package']['obsname'] : $product['package']['name']) : $product['package'];
    return $package;
}


function getPath($arch,$product)
{
    global $pathRoot;
    return isset($product['path'][$arch]) ? $product['path'][$arch] : $pathRoot.":win$arch";
}

function getProjectPath($arch,$product)
{
    $package = getOBSPackageName($product);
    return getPath($arch,$product).$product['project']."/mingw$arch-$package";
}

function getProjectsList($prefix, $suffix)
{
    global $products,$archs;
    $s = "";
    foreach($products as $product) {
        foreach($archs as $arch) {
            $s .= $prefix.getProjectPath($arch,$product).$suffix;
        }
    }
    return $s;
}

function getUrl($arch,$product)
{
    global $urlRoot,$repoRoot;
    return $urlRoot.str_replace(":",":/",getPath($arch,$product).$product['project'])."/$repoRoot/noarch/";
}

function getOBSUrl($arch,$product)
{
    global $obsPackageRoot;
    $package = getOBSPackageName($product);
    return "$obsPackageRoot".getPath($arch,$product).$product['project']."/mingw$arch-".$package;
}

function getDownloadUrl($url)
{
    if (hasRpm()) {
        $hash = sprintf("%d%f", mt_rand(), microtime(true));
        $_SESSION[$hash] = $url;
        return "/unpack.php?key=$hash";
    } else
        return $url;
}

function ref($key)
{
    return "<a href=\"#n$key\">[$key]</a>";
}

function addRef($key, $text)
{
    return "<p id=\"n$key\">[$key]&nbsp;$text</p>";
}

function git_history_url($portableLink,$text)
{
    preg_match("/.*portable-(\d\.\d).*\.([^-]+)-lp.*/", $portableLink, $matches);
    if (sizeof($matches) > 0) {
        $branch = $matches[1];
        $rev = $matches[2];
        return "<a href=\"https://invent.kde.org/office/kmymoney/-/network/$branch?extended_sha1=$rev\">$text</a>";
    }
    return "";
}

header("Content-Type: text/html; charset=utf-8");

echo "<!DOCTYPE html>"
    . "<html><body style=\"background-color:white;\">"
    . "<h1>Download cross compiled $product snapshots for Windows</h1>"
    . (!hasRpm() ? "<p> The files on this page are '.rpm' files. Use <a href=\"https://www.7-zip.de/\">7-zip</a> to extract the binaries.</p>" : "")
    ;

foreach($products as $product) {
    $productName = $product['name'];
    $package = $product['package'];
    $gitUrl = $product['giturl'];
    $anchor = $product['anchor'];
    echo "<h2 id=\"$anchor\">Product: $productName&nbsp;<a href=\"#$anchor\"><small>persistant link</small></a></h2>"
        ."<br/>"
        ;

    foreach($archs as $arch) {
        $url = getUrl($arch, $product);
        $obsUrl= getOBSUrl($arch, $product);
        if (function_exists('curl_init')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $page = curl_exec($ch);
            curl_close($ch);
            if ($verbose == 1)
                echo "<small>$url</small>";
            if ($verbose > 1)
                echo "<small>$page</small>";
            $lines = explode("\n", $page);
            $portableLink = "";
            $setupLink = "";
            $debugPackageLink = "";
            $portableText = "portable package";
            $setupText = "setup installer";
            $debugPackageText = "debug symbol package";
            $baseName = is_array($package) ? $package['name'] : $package;

            foreach($lines as $line) {
                # extract link
                #  href="mingw32-kmymoney-portable-4.8.7f3e468f9-75.3.noarch.rpm"><..."
                #  href="mingw64-kmymoney-portable-4.8.3cf78f9c1-lp151.76.16.noarch.rpm"><..."
                $p2 = is_array($package) ? ".*".$package['version'] : "";
                preg_match("/<a href=\"(.*$baseName-$p2.*\.rpm)\"></", $line, $matches);
                if (sizeof($matches) > 0) {
                    #echo "+++<pre>".$matches[1]."</pre>+++";
                    if (strstr($matches[1], "portable") !== FALSE)
                        $portableLink = $matches[1];
                    if (strstr($matches[1], "setup") !== FALSE)
                        $setupLink = $matches[1];
                    if (strstr($matches[1], "debugpackage") !== FALSE)
                        $debugPackageLink = $matches[1];
                }
            }
            echo "$arch bit<br/>";
            if ($portableLink != '') {

                echo "<ul>"
                    . "<li>download <a href=\"".getDownloadUrl("$url$portableLink")."\">$portableText</a>&nbsp;(compressed archive with portable installation) ".(!hasRpm() ? ref(1) : "")."</li>"
                    . "<li>download <a href=\"".getDownloadUrl("$url$setupLink")."\">$setupText</a>&nbsp;(executable, requires Administrator access) ".(!hasRpm() ? ref(1) : "")."</li>"
                    . "<li>download <a href=\"".getDownloadUrl("$url$debugPackageLink")."\">$debugPackageText</a>&nbsp;(compressed archive) ".(!hasRpm() ? ref(1) : "")." ".ref(2)."</li>"
                    . "<li>get git ".git_history_url($portableLink, "history")." of this package</li>"
                    . "</ul>"
                    ;
            } else {
                echo "<ul><li>No snapshots available yet.</li></ul>";
            }
            $projectPath = getProjectPath($arch,$product);
            echo "<ul>"
                ."<li>visit <a href=\"".$obsUrl."\">OBS</a>&nbsp;package</li>"
                ."<li>see <a href=\"".$gitUrl."\">changelog</a>&nbsp;of related branch</li>"
                ."<li>check out project and build locally: ".ref(3)
                ."<pre>"
                ."osc co $projectPath\n"
                ."cd $projectPath\n"
                ."osc build $repoRoot [-j&lt;jobs&gt;]"
                ."</pre>"
                ."</li>"
                ."</ul>";
        } else {
            echo "<ul>"
                . "<li>choose <b>mingw$arch-$baseName-portable-....rpm</b> to get a zip file with a portable installation of $productName</li>"
                . "<li>choose <b>mingw$arch-$baseName-setup-....rpm</b> to get a setup installer for $productName</li>"
                . "<li>Debug symbols can be found in the file <b>mingw$arch-$baseName-debugpackage-....rpm</b>. They need to be unpacked in the directory above the <b>bin</b> folder
                so that gdb can find them.</li>"
                . "</ul>"
                . "<p>After clicking on the button below you will be redirected to the related download page to get a $productName snapshot</p>"
                . "<input type=\"button\" name=\"xxx\" value=\"$arch-bit snapshot\" onclick=\"javascript:window.location='$url';\">"
                ;
        }
    }
}
echo
    '<h3>Footnotes</h3>'
    .(!hasRpm() ? addRef(1,"The rpm container can be unpacked with <a href=\"https://www.7-zip.de/\">7-zip</a>.") : "")
    .addRef(2, "Debug symbols need to be unpacked in the directory above the <b>bin</b> folder so that gdb can find them.")
    ;
echo "
<h1 id=\"n3\">Local build instructions</h1>
To build the above mentioned packages on a local openSUSE Linux distribution you can follow this recipe:
<ul>
<li>create <a href=\"https://idp-portal.suse.com/univention/self-service/#page=createaccount\">obs account</a>, if not present</li>
<li>install osc:<pre>sudo zypper install osc</pre></li>
<li>checkout and build package as mentioned above at the associated product</li>"
."<li>enter build dir:<pre>osc chroot $repoRoot\ncd /home/abuild/rpmbuild/BUILD/</pre></li>"
;

echo "</body></html>";
session_write_close();
