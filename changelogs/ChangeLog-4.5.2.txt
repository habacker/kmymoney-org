2010-11-14 16:14  tbaumgart

	* CMakeLists.txt: Bumped version number

2010-11-16 08:36  tbaumgart

	* developer-doc/phb/CMakeLists.txt,
	  kmymoney/plugins/icalendarexport/pluginsettings.kcfgc,
	  kmymoney/plugins/printcheck/pluginsettings.kcfgc,
	  kmymoney/x-kmymoney.xml: Added newlines at end of files where
	  missing

2010-11-18 17:22  conet

	* kmymoney/misc/financequote.pl: BUG: 257260
	  Don't show diagnostics from financequote.pl.
	  Merged into the stable branch.

2010-11-20 11:30  conet

	* kmymoney/widgets/kmymoneytitlelabel.cpp: BUG: 257369
	  Improve the page title on the header.
	  Merged into the stable branch.

2010-11-20 14:30  conet

	* kmymoney/views/kgloballedgerview.cpp,
	  kmymoney/widgets/registersearchline.cpp: BUG: 257375
	  Fixed the spacing between the account selector and the 'Search'
	  label. The misalignment described in this bug is caused by the
	  different heights of a line edit and a combo control in the used
	  style. The widgets are all added in a horizontal layout.
	  Merged into the stable branch.

2010-11-22 08:24  tbaumgart

	* kmymoney/views/kbudgetviewdecl.ui,
	  kmymoney/views/kinvestmentviewdecl.ui: Use identical margin for
	  all views

	  BUG: 257582

2010-11-24 19:50  conet

	* kmymoney/views/kmymoneyview.cpp, kmymoney/views/kmymoneyview.h:
	  BUG: 257761
	  Fix the bug and implement the KMyMoney header functionality in a
	  much nicer way. Instead of hiding the standard header and having
	  the KMyMoney header in all the views remove the standard header
	  from the layout and replace it with one instance of the KMyMoney
	  custom header (the text is adapted after a page change).
	  Merged into the stable branch.

2010-11-24 21:17  conet

	* kmymoney/dialogs/settings/ksettingsgeneraldecl.ui,
	  kmymoney/views/kbudgetviewdecl.ui,
	  kmymoney/views/kforecastviewdecl.ui: BUG: 256769
	  Improve the layout of the budgets and forecast views for a better
	  vertical space usage. The only thing that was preventing the
	  usage of KMyMoney on 1024x600 was the settings dialog which is
	  fixed by this commit. For the status and tool bar reported for
	  this bug they can be shown if the KMyMoney header is hidden using
	  the settings.
	  So a user with 1024x600 can hide one of the following 3 items to
	  make KMyMoney fit into 600 pixels in height:
	  - the title widget
	  - the tool bar
	  - the status bar
	  Merged into the stable branch.

2010-11-27 10:15  conet

	* kmymoney/models/accountsmodel.cpp: BUG: 257986
	  Fix the bug plus as an extra feature to display the institution
	  value with the negative color if it's value is negative.
	  Merged into the stable branch.

2010-11-27 10:59  tbaumgart

	* kmymoney/models/accountsmodel.cpp: Show entries in normal color
	  (again) if their balance becomes positive while another account
	  is added.

	  Backported to stable branch

	  BUG: 257986

2010-11-29 15:41  tbaumgart

	* kmymoney/widgets/kmymoneytitlelabel.cpp: Replace \n with blank in
	  title label.

	  Backported to stable branch.

	  BUG: 258257

2010-12-01 07:34  conet

	* kmymoney/views/kpayeesview.cpp: BUG: 258466
	  Select the payee when the current payee was changed in the list.
	  Merged into the stable branch.

2010-12-01 07:47  conet

	* kmymoney/views/kmymoneyview.cpp, kmymoney/views/kmymoneyview.h:
	  Fixed compiler warnings. Merged this fixes into the stable branch
	  since the code that generated the warnings was also merged into
	  this branch.

2010-12-01 08:01  conet

	* kmymoney/views/kforecastviewdecl.ui: BUG: 258460
	  Reduce the application's minimum horizontal size. This does not
	  completely remove the minimum horizontal size it just makes it
	  smaller. The minimum horizontal size is determined by the widest
	  view which is 'Forecast' so in the 'List' view mode that minimum
	  size is 1008 (using the Romanian translation). That is enough
	  even to fit into a netbook's 1024x600 resolution.
	  Merged into the stable branch.

2010-12-01 17:15  conet

	* kmymoney/dialogs/transactioneditor.cpp: BUG: 258290
	  Disable the split button until a proper account was selected. The
	  code is there on the KDE3 version don't know how it got lost
	  during the port.
	  Merged into the stable branch.

2010-12-02 16:47  conet

	* kmymoney/dialogs/transactioneditor.cpp: BUG: 258290
	  Disable the split button only if the editors account is not a
	  valid one like in a new scheduled transaction when an account is
	  not yet selected.
	  Merged into the stable branch.

2010-12-05 11:54  conet

	* kmymoney/dialogs/kequitypriceupdatedlg.cpp: BUG: 258610
	  Disable the sorting while the update is running to maintain the
	  items order at the start of the update process on which the
	  update depends.
	  Merged into the stable branch.

2010-12-07 20:57  conet

	* kmymoney/widgets/register.cpp,
	  kmymoney/widgets/transactionform.cpp: BUG: 258355
	  Hide the edit widgets before removing them because if the widgets
	  are visible while they are being removed (actually scheduled for
	  deleteLater()) some events will be sent to the register (during
	  the actual delete process) which will cause a crash by accessing
	  the widgets which where already deleted. This was happening
	  somewhere deep in Qt's code and I found this to be the best
	  workaround for it.
	  Merged into the stable branch.

2010-12-10 08:03  tbaumgart

	* kmymoney/widgets/kmymoneylineedit.cpp: Prevent recursive loop

	  Backport to stable branch

	  BUG: 259369

2010-12-10 09:39  tbaumgart

	* kmymoney/widgets/transactionsortoptionimpl.cpp,
	  kmymoney/widgets/transactionsortoptionimpl.h: Fix sort order
	  option dialog's focus handling

	  Backported to stable branch

	  BUG: 259179

2010-12-10 22:38  conet

	* kmymoney/models/accountsmodel.cpp: BUG: 259436
	  Implement sorting the accounts by value in the accounts model.
	  Merged into the stable branch.

2010-12-10 23:00  conet

	* kmymoney/views/khomeview.cpp: BUG: 259437
	  Use adjustedNextDueDate when splitting schedules into groups in
	  the home page to avoid having the same instance of the
	  transaction in two groups if the 'Enter transaction on another
	  day' option is used.
	  Merged into the stable branch.

2010-12-10 23:11  conet

	* kmymoney/converter/mymoneyqifreader.cpp: BUG: 259235
	  Don't crash if there is no valid date format selected.
	  Merged into the stable branch.

2010-12-12 13:27  tbaumgart

	* kmymoney/converter/mymoneygncreader.cpp,
	  kmymoney/mymoney/mymoneyprice.cpp: Don't import zero prices from
	  GnuCash
	  Print more details about the price to the console

	  Backported to stable branch

	  BUG: 256282

2010-12-12 14:01  tbaumgart

	* kmymoney/converter/mymoneystatementreader.cpp: Delete transaction
	  editor upon an exception

	  Backported to stable branch

	  BUG: 256787

2010-12-12 15:32  tbaumgart

	* kmymoney/converter/mymoneygncreader.cpp,
	  kmymoney/dialogs/kfindtransactiondlg.cpp, kmymoney/kmymoney.cpp,
	  kmymoney/models/accountsmodel.cpp, kmymoney/models/models.cpp,
	  kmymoney/mymoney/mymoneyprice.cpp,
	  kmymoney/views/kmymoneyview.cpp,
	  kmymoney/widgets/transactionsortoptionimpl.cpp: Ran astyle

2010-12-12 17:58  tbaumgart

	* kmymoney/converter/webpricequote.cpp: Detect and use correct
	  character decoder when scanning web-pages for price information

	  Backported to stable branch

	  BUG: 259605

2010-12-13 04:40  conet

	* kmymoney/views/kmymoneyview.cpp: Merged into the stable branch.

2010-12-13 05:01  conet

	* kmymoney/views/kscheduledview.cpp: Fix the display of the
	  translated schedule frequency in the schedules view.
	  Merged into the stable branch.

2010-12-15 09:00  tbaumgart

	* kmymoney/converter/mymoneystatementreader.cpp: Make sure to
	  delete the transaction editor before the dialog in any case to
	  avoid crashes.

	  Backported to stable branch

	  BUG: 259916

2010-12-15 18:56  conet

	* kmymoney/reports/CMakeLists.txt: Added dependency to fix build
	  with make -j > 1.
	  Merged into the stable branch.

2010-12-16 07:32  conet

	* kmymoney/dialogs/settings/CMakeLists.txt: Add missing dependecies
	  for parallel build.
	  Merged it into the stable branch for the upcomming release to be
	  available on Windows also.

2010-12-18 21:10  tbaumgart

	* kmymoney/widgets/register.cpp: Fix selection of multiple register
	  items with the keyboard

	  Backported to stable branch

2010-12-20 07:52  tbaumgart

	* kmymoney/plugins/ofximport/ofximporterplugin.cpp: Don't count
	  empty lines in OFX file when verifying the format

	  Applied fix dated 2009-08-03 from KMyMoney 1.0 repository

2010-12-20 18:12  conet

	* kmymoney/widgets/CMakeLists.txt: Fix parallel build.

2010-12-20 18:20  conet

	* kmymoney/dialogs/settings/ksettingsreports.cpp,
	  kmymoney/dialogs/settings/ksettingsreports.h,
	  kmymoney/reports/reporttable.cpp, kmymoney/reports/reporttable.h,
	  kmymoney/widgets/transactionform.h: Removed including
	  kmymoneysettings.h and kmymoneyglobalsettings.h from all header
	  files to avoid unneeded build dependency between files. Please
	  don't use these includes in kmymoney headers in the future.

2010-12-21 12:36  tbaumgart

	* kmymoney/mymoney/mymoneybudget.cpp: Fix budget error.

	  Backport from trunk.

2010-12-21 14:33  tbaumgart

	* kmymoney/views/kbudgetview.cpp, kmymoney/views/kbudgetview.h:
	  Don't allow to select budgets if they are included by a superior
	  account. We still need to add logic to support multiple
	  currencies. I left a TODO mark in the code.

	  Backported to stable branch

	  BUG: 255135
