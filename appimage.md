---
title: "the BEST Personal Finance Manager for FREE Users, full stop."
layout: page
---

# How to download and use the AppImage package

AppImage packages are availble for Linux users who want to use a version newer than the one available in their Software Center.<br>
They offer the most recent release including all the latest fixes and are generated daily straight from the stable branch of our source code.

You will need to:
* Download the package file from the [binary factory](https://binary-factory.kde.org/job/KMyMoney_Stable_Appimage_Build/) to a location of choice.

* Try opening the file directly from that location simply by double-clicking it. If that doesn’t work, continue with the next steps.

* Open a terminal program (e.g. Konsole) and execute these commands, each followed by pressing the Enter/Return key:

  * Change into the directory where you downloaded the file, e.g.: `cd ~/Download`

  * Make the file executable: `chmod +x name-of-downloaded-file`

  * And execute it with: `./name-of-downloaded-file`
    - The dot and the slash are important!

For the more adventurous Linux users, the nightly AppImage packages with the latest development testing 
changes are also [available](https://binary-factory.kde.org/job/KMyMoney_Nightly_Appimage_Build/).
