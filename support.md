---
title : "KMyMoney - Support"
layout: page
---

# How can we help you?

KMyMoney comes with a lively community and several options are available if you're looking for help:

* A very comprehensive [**User's Manual**](documentation.html)

  Recommended especially for new users. Read all about the KMyMoney's functionality – maybe even in your own language!

* [**Frequently Asked Questions**](https://userbase.kde.org/KMyMoney/FAQ)
  
  Make sure to check out our FAQ before you proceed with any of the other options listed below.

* [**Userbase Wiki**](https://userbase.kde.org/KMyMoney) – this one is for regular users.

* [**GitLab Wiki**](https://invent.kde.org/office/kmymoney/-/wikis/home) – and this one for developers.

* Our official [**Discussion Forum**](https://forum.kde.org/viewforum.php?f=69)

* You can access our Chat:

  - Using [**Telegram group**](https://t.me/kmymoney)
  
  - Using [**IRC WebChat**](http://webchat.freenode.net/?channels=kmymoney)
  
  - Using [**IRC client**](irc://irc.freenode.net/kmymoney)
  
* [**Users Mailing List**](https://mail.kde.org/mailman/listinfo/kmymoney)
  
  Anyone is welcome. An [archive](https://marc.info/?l=kmymoney) is also available.

* [**Developers Mailing List**](https://mail.kde.org/mailman/listinfo/kmymoney-devel)
  
  This one is for developers. An [archive](https://marc.info/?l=kmymoney-devel) is also available.

* [**Report a Bug**](https://bugs.kde.org/enter_bug.cgi?product=kmymoney&format=guided) 

  Found a bug? Report it! Our tool will suggest any potential duplicates, so please make sure to go through them first before you submit yours. 

* [**Suggest a Feature**](https://bugs.kde.org/enter_bug.cgi?product=kmymoney&format=guided) 

  Missing some functionality? Open a wishlist item.

* [**Recovery key**](recovery.html) 

  To prevent accidental lockout from your encrypted KMyMoney file you can use the recovery key.

