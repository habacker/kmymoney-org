commit 52a838f7347cda40608f028952cc107bd47bd2a8
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Apr 21 13:17:03 2019 +0200

    Bumped version to 5.0.4

commit 8ec3540e4229c8fe616946b8400a558e66c5f05c
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Apr 21 11:17:32 2019 +0200

    Cleanup display of budget block if no data is available

    This amends commit a3f3aeed17a91f9ab7a6a445880b949ae57aeba3

commit f9fdfce0b07aff9e24589381f81ccbfff07ab55e
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Apr 20 22:58:41 2019 +0200

    Fix loading of report attribute "includingBudgetActuals"

    The budget report attribute "includingBudgetActuals" was not handled
    correctly when a file was loaded into memory.

    Existing files showing a problem cannot be fixed automatically. The
    respective reports need to be recreated by the user.

    Also, some more attributes needed a fixed seed to solve issue #396978.

    BUG: 406608
    FIXED-IN: 5.0.4

commit a3f3aeed17a91f9ab7a6a445880b949ae57aeba3
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Apr 20 18:45:57 2019 +0200

    Fix budget header on home view

    The header was shown twice. This change removes the extra instance.

    BUG: 406714
    FIXED-IN: 5.0.4

commit 7753b10ccd3f3efe4d9b22173c2b5368ecfc972e
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Apr 20 18:45:05 2019 +0200

    Use correct column to extract budget values for home page

    BUG: 405828
    FIXED-IN: 5.0.4

commit 600af541035959ace4bdd21b74d078764f02516e
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Apr 20 07:49:04 2019 +0200

    Make XML files comparable again

    Since commit e4d384d34a91eb6024800 (which later made it into v5.0.2),
    the order of attributes in the XML file is random each time the file is
    saved. This leads to the fact that it is extremly hard to compare two
    files because one does not see the forest for the trees.

    This change removes the randomness and thus brings back comparability.

    BUG: 396978
    FIXED-IN: 5.0.4

commit c26325518badccf7487a04d12d0bc913ddacc405
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Fri Apr 19 19:44:45 2019 +0200

    Added small handy script tools

commit bd01a5ce650a5e9a13db3e42791fd9b0fed65052
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Fri Apr 19 15:31:02 2019 +0200

    Allow creating of accounts during statement import

    If a user tries to create an account during statement import, the
    account was not created at all.

    This change makes sure that the enclosing engine transaction is only
    opened after the account is selected. Doing that too early causes the
    creation to fail.

    BUG: 403745
    FIXED-IN: 5.0.4

commit 9fca9e3661ce2d03e472ab1d520b59493ec110cb
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Fri Apr 19 10:10:35 2019 +0200

    Provide internal OFX id generation also for WebConnect

    The change provided in commit 65e3837697c93786 is not available when
    importing an OFX statement file via WebConnect. For this case, a new
    option has been added (no UI yet) to allow selection of the default for
    the unique transaction id to be either the OFX FITID field or our
    internal hash. The default is to use OFX FITID, so no change in prior
    behavior. This default settings will also be used to preset the combo
    box in the file selection dialog for OFX import and the account setting
    for mapped accounts.

    If users set this option, they need to know that some duplicate
    transactions may appear since the method to detect them changed. One way
    to deal with that is to remove both transactions and redo the import.

    To set the method to the internal hash create a file named

      ~/.config/kmymoney/ofximporterrc

    with the following content:

      [General]
      useOwnFITID=true

    Future versions of KMyMoney will provide a UI to change this setting.

    CCBUG: 390681

commit 1ef147cdc5bab0a84bb30f3af1b8355c38fe433e
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Fri Apr 19 09:55:33 2019 +0200

    Show all asset and liability accounts during statement import

    This change is a workaround to allow access to all asset and liability
    accounts despite the information provided in the data from the
    institution (which we have learned in some cases is just wrong).

    A better approach would be to have a UI option to enhance the list does
    not contain the account the user is looking for.

    BUG: 396225
    BUG: 392305
    FIXED-IN: 5.0.4

commit f328d389f6af43cadf16fd094f2cbfd12e020d22
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu Apr 18 07:56:56 2019 +0200

    Keep file permissions when replacing existing file

    BUG: 40139
    FIXED-IN: 5.0.4

    Differential Revision: https://phabricator.kde.org/D20646

commit a366b51e40a6f7a4d9d1b06b07e677892313c60e
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Apr 14 19:36:36 2019 +0200

    Do not re-encrypt file

    This change allows to remove encryption permanently.

    BUG: 406537
    FIXED-IN: 5.0.4

commit 390561a6400ecbbf6deed7c0928ed1968a45a665
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Apr 14 13:52:39 2019 +0200

    Allow change the zoom factors for reports

    This allows to change the report shown to be zommed using the ctrl-key
    and the mouse-wheel. The modification is not (yet) persistent.

commit 4b7bbe834886c5ebe7fba86a68290dec4c1850bd
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Apr 14 13:50:52 2019 +0200

    Fix subtotals

    Subtotals in e.g transaction reports are not correctly calculated and
    shown. This only shows up if a the category type (e.g. income, expense)
    and the sub-category name change and the top-level category name is the
    same.

    This change fixes the problem so that all sub-totals are calculated and
    presented correctly.

    BUG: 406525
    FIXED-IN: 5.0.4

commit cfd4b81c81ed43fb36ebb35ac3d5e7a4723ba9a8
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Apr 14 07:35:12 2019 +0200

    Set focus of find transaction dialog to text entry field

    This change allows to enter a text in the text search field and pressing
    Return to start the search.

    BUG: 406509
    FIXED-IN: 5.0.4

commit 4f18e693e29317d59bc3faa49f8a600df22f7a49
Author: Yuri Chornoivan <yurchor@ukr.net>
Date:   Sun Apr 7 21:24:03 2019 +0300

    More typo fixes

commit a761bd596dfa6cdcea8d94a2468592a23fa139ab
Author: Yuri Chornoivan <yurchor@ukr.net>
Date:   Sun Apr 7 16:24:43 2019 +0300

    Fix minor typos

commit 50e7eb27deceb6f2c818f4c4b8ea3b39e173bfe0
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Apr 7 14:53:18 2019 +0200

    Enable printing of charts

    Ported from 2ac6eb5433f81111ae7206e84a2cfda50ddcfe7f

commit 37ee0de894992f6d4d77a4a40ed0903b99bfeea9
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Apr 7 13:00:56 2019 +0200

    Do not throw away schedule data in case of error

    Keep data and re-edit schedule in case the schedule cannot be added to
    the database.

    BUG: 405928
    FIXED-IN: 5.0.4

commit 355ba2656cd33340d3ce6bc4d4325b71145d97b7
Merge: 1c89fa684 65e383769
Author: Jack Ostroff <ostroffjh@users.sourceforge.net>
Date:   Sun Apr 7 08:21:53 2019 -0400

    Merge branch '5.0' of git.kde.org:kmymoney into 5.0

commit 1c89fa68469ff2bf25984ab7d1e81e61194a2af9
Author: Jack Ostroff <ostroffjh@users.sourceforge.net>
Date:   Sun Apr 7 08:20:41 2019 -0400

    Final doc updates for 5.x prior to release of 5.0.4

commit 65e3837697c9378616a7eda2ba3387d1208f288e
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Apr 7 09:12:22 2019 +0200

    Provide method to construct unique transaction ID for OFX import

    User reports show, that the FITID attribute provided by some
    institutions during OFX statement download is not as unique as it is
    specified. In fact, some banks issue a different FITID every time you
    download the same statement.

    This makes the FITID attribute absolutely worthless for duplicate
    detection.

    Since asking banks to fix their software has no reasonable chance to
    lead to success on short call, this change allows to switch the
    duplicate detection to be based on a method provided by KMyMoney. In
    fact it uses the same algorithm which works well in the KBanking
    importer for years already. Note: switching from "OFX FITID" to
    "KMyMoney Hash" may result in duplicates for one more time.

    At the same time, the OFX import options are now available during file
    import and not only for mapped accounts (OFX direct connect).

    BUG: 390681
    FIXED-IN: 5.0.4

commit cbc5a3e1238df7f4bcf90ee3df4280ba03c84ecc
Author: Jack Ostroff <ostroffjh@users.sourceforge.net>
Date:   Sat Apr 6 18:23:21 2019 -0400

    More doc updates for 5.x
    updating author (committed from unsetup laptop)

commit dae72c5e5ed7867bb54a42f42339c895a77b5cb9
Author: Pino Toscano <pino@kde.org>
Date:   Sat Apr 6 08:33:55 2019 +0200

    doc: use more entities

commit a4a39821bbea41228f6f7ee49d1d97f6f006ea8e
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu Apr 4 19:59:06 2019 +0200

    Avoid crash when removing items from container

    BUG: 406220
    FIXED-IN: 5.0.4

commit de5118bee6cd0b653b89f4705f159a1a4098f77e
Author: Yuri Chornoivan <yurchor@ukr.net>
Date:   Wed Apr 3 13:32:23 2019 +0300

    Fix minor typos

commit 82824a3fac08bd1d494f859a6389ec54c1c127a4
Author: Jack Ostroff <ostroffjh@users.sourceforge.net>
Date:   Tue Apr 2 15:35:51 2019 -0400

    Update Ledgers chapter for 5.x

commit 1d8fe6a1899acb45d491605c093b5ac1c66cb59e
Author: Jack Ostroff <ostroffjh@users.sourceforge.net>
Date:   Tue Apr 2 13:18:30 2019 -0400

    More doc updates for 5.x: payees and schedules
    More minor edits: settings and new png
    Updated but not reviewed by me yet: ledgers

commit ac2b6d7cfd7aef77af6c9559becc7b2740e70467
Author: Yuri Chornoivan <yurchor@ukr.net>
Date:   Tue Apr 2 18:42:49 2019 +0300

    Use dot instead of comma

commit a307901f6c0e053fd1e30383c2b3c3944b47a2da
Author: Yuri Chornoivan <yurchor@ukr.net>
Date:   Tue Apr 2 16:33:47 2019 +0300

    More typo fixes

commit ba406d2b753b3a23e52206f9f1b8a8621bc98de6
Author: Yuri Chornoivan <yurchor@ukr.net>
Date:   Tue Apr 2 10:34:24 2019 +0300

    Fix typos, use entities

commit f2f889ee21b97192fc01710c9b110730f324bdb2
Author: Jack Ostroff <ostroffjh@users.sourceforge.net>
Date:   Mon Apr 1 17:20:28 2019 -0400

    additional minor edits to Settings chapter.

commit 02c7b64b72a97233a8bfeddf78d4661d41b2a61b
Author: Pino Toscano <pino@kde.org>
Date:   Mon Apr 1 22:32:05 2019 +0200

    doc: few more changes

    Mostly entities.

commit 0f85e6f84e212faf6217dd7db631741dda2a77fc
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Apr 1 22:19:39 2019 +0200

    Fixed spelling in a dialog

    GUI:

commit acbdb38736b371e2f2d445a4635c241c26d5c713
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Apr 1 22:18:11 2019 +0200

    Fixed a few typos in the recent documentation changes

commit 05a840775bb3a15680b1f41eed9573ec17a0db9d
Author: Jack Ostroff <ostroffjh@users.sourceforge.net>
Date:   Mon Apr 1 15:31:23 2019 -0400

    minor docbook updates

commit cc16ad78f8d2685b6d208d5f1ca9a10c944491d8
Author: Michael Carpino <mfcarpino@gmail.com>
Date:   Mon Apr 1 15:30:24 2019 -0400

    Update Settings docbook chapter for 5.x

commit 66157f2d365b8c8259730c8749161cb0aad4c033
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Mar 31 13:45:48 2019 +0200

    Forecast (history) has no effect on home view

    The home view section selector in the settings has an entry "Forecast
    (history)" which does not show any effect on screen when changed. Since
    it is currently not used, it is removed from the settings list. It will
    be kept in source code (marked as unused) since the order of the entries
    and their index must not change to maintain backward compatibility of
    the settings.

    BUG: 406074
    FIXED-IN: 5.0.4

commit d404c54520599ee67c88c273a0a20bcdf09fe9eb
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Mar 31 13:10:07 2019 +0200

    Update forecast method after change of settings

    BUG: 406073
    FIXED-IN: 5.0.4

commit c89b7ff9d2864942cb01bcd7b0faf7fef54095a1
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Mar 25 17:42:29 2019 +0100

    Removed unused #include statements

commit c4a8edc6566bb1c36e61e3fef786b538fe9a839f
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Mar 24 09:03:25 2019 +0100

    Use a global printer object for all print jobs

    Summary: BUG: 405059

    Differential Revision: https://phabricator.kde.org/D20010

commit abfeb37dfaeeb73fb9129264725968da8ad97f94
Author: l10n daemon script <scripty@kde.org>
Date:   Mon Mar 25 08:54:27 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours

    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit 03e9da796f356c7e0b6406a66c4222410ce8e56c
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Mar 24 16:09:16 2019 +0100

    Treat footer lines in CSV import relative to EOF

    BUG: 405817
    FIXED-IN: 5.0.4

commit c64a8c06e0a162aaf1030ed0fd99f6e01b67963d
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Sun Mar 24 14:17:18 2019 +0100

    Add support for assigning tags in transaction splits editor

    In the transaction split editor the tags column is designed similar
    to categories and shows the assigned tags as text.
    On editing a split a drop down box with all available tags and
    checkboxes for removing assigned tags are visible.

    Because tags are always assigned to splits, adding tags by the transaction
    split editor works in the same way as adding tags in the ledger view.

    Please note that the report engine is not able to filter
    tags on categories yet.

    Ported to KF5 from commit c712d81154ef2fd5bb136bc4521ba3d046b74ba6.

    BUG:341589
    FIXED-IN:4.8.4,5.0.4
    Reviewed By: tbaumgart
    Differential Revision: https://phabricator.kde.org/D18789

commit 1e3a667646116d2ad886e145ad1d0a15914cc604
Author: l10n daemon script <scripty@kde.org>
Date:   Thu Mar 21 08:46:04 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours

    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit 045d74b680a7fc691690d6c35c76d32053602553
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Mar 19 10:21:33 2019 +0100

    Avoid using KIO on local files

    Using KIO on MacOS and Windows may cause some trouble and is not
    required when working on local files. This change tries to bypass the
    KIO functions by using Qt standard functions in this case.

    BUG: 400761
    FIXED-IN: 5.0.4

commit f4c359840e16d6c91dc7db8480209c56557046cc
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu Mar 14 18:32:45 2019 +0100

    Improve automatic reconciliation

    It took way too long for the number of combinations to calculate. The
    total of combinations checked has been reduced from 300.000 to 60.000
    before the search is terminated. Also, the progress bar has been fixed
    so that there is some visual feedback to the user during the
    calculations.

    BUG: 405329
    FIXED-IN: 5.0.4

commit 6deaf7ff9655e341f8ee03a0989d2be43c8b8a14
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu Mar 14 18:27:42 2019 +0100

    Fix progress bar functionality

    Also reduce waiting period to 200ms

commit c76d1f317df0592fd79d48675e109df4a43730b3
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu Mar 14 18:26:46 2019 +0100

    Remove unused code

commit d52b8a2fd58c9d87c6c5c28f84ebd492e96e3e23
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Mar 10 14:41:58 2019 +0100

    Store Windows DLLs in bin instead of lib

commit 12d7b4656a4a301eda2ffd0e8a27cc3aece4d983
Author: l10n daemon script <scripty@kde.org>
Date:   Sun Mar 10 08:29:02 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours

    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit 9d9c9a77e9c45612b230ed2fac2064d51d7e3be7
Author: l10n daemon script <scripty@kde.org>
Date:   Sun Mar 10 07:24:49 2019 +0100

    GIT_SILENT made messages (after extraction)

commit 59afca03c97166f7b63b8c54710ce130b9b3378f
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu Mar 7 09:05:37 2019 +0100

    Collect generated binaries in distinct locations

commit 57633fadcbfd316de7396cdf7b4ca295cc4eba2c
Author: l10n daemon script <scripty@kde.org>
Date:   Thu Mar 7 08:39:20 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours

    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit fc82b71c9a1e71ed154405626aa5b78ebd625041
Author: José Arthur Benetasso Villanova <jose.arthur@gmail.com>
Date:   Tue Mar 5 18:12:08 2019 +0100

    TAB navigation is not working in budget screen

    Summary:
    FEATURE: 405038

    Reviewers: #kmymoney, tbaumgart

    Reviewed By: #kmymoney, tbaumgart

    Differential Revision: https://phabricator.kde.org/D19492

commit 1baeee6d474c99da2308ccc5ae631756b3e6e739
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Mar 5 16:33:50 2019 +0100

    Fix crash while importing transactions and matching schedules

    This amends commit 9f702f34e9a6197f7d526467505ddaf56f6c5c1e such that it
    supports a pointer to the parent widget being the nullptr

commit 0467a80113e19290299c6ad11ac4e0145b363bba
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Mar 5 13:28:30 2019 +0100

    Resolve problems reported by coverty scan

commit 9490b13ca62af4b8595f8e879e7a1c9fad4e8899
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Mar 5 12:59:44 2019 +0100

    Use qrand() instead of rand()

    Found another one

commit 82b12bc7d3b0c591e64e1cf5df6f7dd1ae5d2f0a
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Mar 5 12:54:15 2019 +0100

    Fixed duplicate name issue in ui file

    The UI compiler took care of it but issued a warning. This is now
    resolved.

commit 510d6ec81a883779e1592bfd77807d74ab9a3eab
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Mar 5 09:40:15 2019 +0100

    Use qrand() instead of rand()

    This should solve a problem reported by coverty scan

commit 9f702f34e9a6197f7d526467505ddaf56f6c5c1e
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Mar 5 08:36:18 2019 +0100

    Remove call to winId

    A call to winId causes a crash on MS-Windows. It was needed in certain
    circumstances to place the dialog centered on the application window.
    This is now handled by a separate move() of the dialog to the position.

    BUG: 404848
    FIXED-IN: 5.0.4

commit d03dd8e1b5303a8292401047c2a725fb9c4ca024
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Mar 2 10:27:12 2019 +0100

    Set precision of edit widget before loading value

    If used the other way around, the value loaded will be rounded to the
    standard precision of 2 digits.

    BUG: 403885

commit 67b97ecc24caaaffeab8bced149f13831de70c7e
Author: l10n daemon script <scripty@kde.org>
Date:   Sat Mar 2 08:36:24 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours

    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit 88cad4dc6c70875a472275b653b641aa5dfc2509
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu Feb 28 11:11:56 2019 +0100

    Use active window as parent for scheduled transaction entry dialog

    Try to resolve the 'Crash on "Enter Next Transcation"' bug

    CCBUG: 404848

commit c8fd1e5534228cf1e93ad858368be72fe21cf187
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Feb 26 20:09:26 2019 +0100

    Fix tab handling in currency calculator dialog

commit 36a63ade6ca0ffc80b6a036c14b39a7e396035ff
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Feb 24 15:32:57 2019 +0100

    Use QT_VERSION_CHECK macro instead of encoded numbers

commit b18f8a15166e32b0ce07f73780ad6a0eff6ff0fc
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Feb 24 15:30:13 2019 +0100

    Fix some problems reported by coverty-scan

commit 71b19a079f1b09d34f72de8adff78563c5ae65e5
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Feb 23 14:44:24 2019 +0100

    Improve usage of QString.arg with multiple string arguments

commit d643cc34aa7304ee3925e79d497f01a201b2791b
Author: l10n daemon script <scripty@kde.org>
Date:   Mon Feb 25 08:38:32 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours

    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit f7b3ebe576d63992ebbb4190f4eafe4c96bc9b06
Author: l10n daemon script <scripty@kde.org>
Date:   Mon Feb 25 07:32:06 2019 +0100

    GIT_SILENT made messages (after extraction)

commit d972bc7d55f3a9df109330203520f00de0f60347
Author: l10n daemon script <scripty@kde.org>
Date:   Sun Feb 24 08:18:39 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours

    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit a9b6160f3cbd1c440ae9aff515dbf07132a0eeed
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Feb 23 14:37:29 2019 +0100

    Improve date change feature

    The current behavior of the + and - key in a date selection widget was
    to increase or decrease by day and roll over into the next resp. previous
    month. This has been enhanced to increase/decrease by day/month/year
    depending on the current section holding the cursor with the month doing
    a roll over into the next/previous year.

    BUG: 399681

commit df44f5a1f9af10a0982ff7c1e65b61af5377ded8
Author: l10n daemon script <scripty@kde.org>
Date:   Fri Feb 15 09:16:14 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours

    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit e848619623c514e0f8bf0b1a9e569f9fd82437ad
Author: l10n daemon script <scripty@kde.org>
Date:   Thu Feb 14 08:37:36 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours

    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit 8f102516e7f6d56e8f6ab5772d5f32048f5dee4a
Author: l10n daemon script <scripty@kde.org>
Date:   Wed Feb 13 08:49:55 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours

    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit cc119c58deaf77ead758e3b3ae729663f9362a78
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Feb 10 12:19:39 2019 +0100

    Improve memo column selection in CSV import wizard

    The CSV import wizard did allow to select multiple columns for memo
    fields but it was not visible to the user which ones were selected.
    Also, there is no way to clear the memo selection only but only all
    column assignments.

    This change adds a display item that shows the selected columns and also
    adds a clear button to clear the memo field selection only.

    GUI:
    BUG: 404156
    FIXED-IN: 5.0.4

commit 3d50b146b40285cbc140b435c4a22250e74ac4d7
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Feb 9 15:46:07 2019 +0100

    Resolved a bunch of level 0 and 1 warnings reported by clazy

commit d292de0b7f976d38d7c4e966aff573d7bb88fdd6
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Feb 9 14:15:05 2019 +0100

    Set precision of amount edit fields for fees and interest

    BUG: 400761

commit ae54e8327de6b04cdf35d21a4c424dc0e9a0561c
Author: l10n daemon script <scripty@kde.org>
Date:   Sat Feb 9 08:56:23 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours

    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit 1ed8708bd03c1af86ea4572910f931f32951b6bd
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Fri Feb 8 19:32:24 2019 +0100

    Allow to copy transaction details to clipboard

    Using the standard copy action (default Ctrl+C) allows to copy the
    details of the selected transactions in the ledger view to the
    clipboard.

commit a5e5f698bcecb12720768bbe8249722468f0354f
Author: l10n daemon script <scripty@kde.org>
Date:   Thu Feb 7 08:22:24 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours

    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit 7eed17c8645fe5beff6c830e7d09d8dc9f662aa7
Author: l10n daemon script <scripty@kde.org>
Date:   Wed Feb 6 08:31:50 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours

    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit eabc71b5952532bf95ec64c2edce9b1233303e1e
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Feb 5 16:12:47 2019 +0100

    Keep position in home view after entering transaction

    BUG: 403955
    FIXED-IN: 5.0.4

commit cf3696a00f32059d4bd0f5b63426bfb696a7113b
Author: l10n daemon script <scripty@kde.org>
Date:   Mon Feb 4 08:19:02 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours

    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit 2fe372e97b012442f6f5be462ee23ebfcd19a5ab
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Feb 3 19:46:17 2019 +0100

    Added automatic fix to adjust settings of precious metals

    Files created before Jan 19th 2019 will most certainly have a false
    setting for the smallest account fraction of the precious metals gold,
    platinum, palladium and silver.

    This change fixes the problem in the file when the file is opened.

    At the same time it initializes an investment transaction with the
    commodity of the account and not the base currency.

    BUG: 403885
    FIXED-IN: 5.0.4

commit 83a35dca6c94b5055f7f65407089e62e2aee3058
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Feb 3 16:48:52 2019 +0100

    Improve detection of empty transaction in register

commit 60e2d8cfd3a774b360ce8aadbd993c50fed404ef
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Feb 3 14:33:46 2019 +0100

    Do not allow to enter transactions prior to account opening date

    The transaction editor in the standard account ledger does not allow to
    enter transactions that reference accounts that have an opening date
    later than the transaction date.

    The investment transaction editor is missing this feature and one could
    enter transactions with a date prior to the opening date of the
    investment or asset account referenced in the transaction.

    Moreover, the opening date of an invest account cannot be changed by the
    user but got updated to a matching value when the consistency check was
    run (e.g. as part of file save). The user was informed and was
    surprised.

    This change adds an automatic update of the opening date for the invest
    account if needed when the transaction is entered. Also, it does not
    allow to enter the transaction, colorizes the date and presents a
    tooltip on the date widget just like for the standard transaction
    editor.

    BUG: 403886
    FIXED-IN: 5.0.4

commit 41d11c7e222393163a025ae2301359655c504e6e
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Feb 3 14:23:59 2019 +0100

    Improved usage of empty strings

commit 55bfc0b30ba862d70703fdce35451b84fbb8a9f9
Author: l10n daemon script <scripty@kde.org>
Date:   Sun Feb 3 08:45:15 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours

    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit 4d56d88b205544f755757adca5036042d862c74d
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Feb 2 22:51:47 2019 +0100

    Try to fix the appimage build

commit 8a7fc5bd4952b7354aca34bc47119d37dbf154d7
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Feb 2 10:20:28 2019 +0100

    Make sure we set VERSION so linuxdeployqt/appimagetool knows how to name the appimage.

    Previously it did not do this, and we had our own logic to rename the
    appimages (which is now unnecessary, and is removed in this fix)

    Ref T10428

    (cherry picked from commit e3c27df58f0a958b949b0aa0a9a6bfe905e80836)

commit b7368fba5aed7ec0781f60950def20133cba246b
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Feb 2 10:15:32 2019 +0100

    Fix reports to show unassigned transactions

    This also fixes some problems with the transaction report by tags

    BUG: 403826
    BUG: 368159
    FIXED-IN: 5.0.4

commit 3ae53b4132f3d2ea6aa1635a22b5a0d0d7329217
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Feb 2 09:40:33 2019 +0100

    Avoid chained assignment to improve code quality

commit 803b03434b5c2254fbbcf6558619eb190dd2e829
Author: l10n daemon script <scripty@kde.org>
Date:   Sat Feb 2 08:16:17 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours

    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit 0b8ebec1b3789ba1d63810df6ac9f1fdad6f2a7e
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Fri Feb 1 16:04:59 2019 +0100

    Implement persistence of validity filter

    BUG: 403825
    FIXED-IN: 5.0.4

commit e052f1830394244c9ca243d3e446a525573266b4
Author: Luigi Toscano <luigi.toscano@tiscali.it>
Date:   Fri Feb 1 01:21:53 2019 +0100

    Rename the json.in files as json.cmake (fix i18n)

    Summary:
    The translation infrastructure of KDE handles only .json
    and .json.cmake files. Renaming the files allow them to be
    translated, without any other side-effect.

    Test Plan: It compiles and the output seems to be the same.

    Reviewers: tbaumgart

    Reviewed By: tbaumgart

    Subscribers: #kmymoney

    Differential Revision: https://phabricator.kde.org/D18636

commit 778ff2c559552f461a0d30ae60abd68c7f5d55c6
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Jan 21 16:58:37 2019 +0100

    Improve result information for OFX mapping

    The source used to obtain information necessary for OFX mapping
    (www.ofxhome.com) also presents information about the availability of
    the provided URL and the verification of the SSL/TLS certificate.

    Since this information was not interpreted by KMyMoney, URLs were
    presented that are known to be broken.

    This change shows an error message when a non working URL or an invalid
    certificate is found on ofxhome.com.

    GUI:
