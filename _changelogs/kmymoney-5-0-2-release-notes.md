---
title: '        KMyMoney 5.0.2 Release Notes
      '
date: 2018-10-17 00:00:00 
layout: post
---

<p>The KMyMoney development team is proud to present <a href="http://download.kde.org/stable/kmymoney/5.0.2/src/kmymoney-5.0.2.tar.xz.mirrorlist">version 5.0.2</a> of its open source Personal Finance Manager.</p><p>Although several members of the development team had been using version
        5.0.1 in production for some time, a number of bugs and regressions slipped
        through testing, mainly in areas and features not used by them.</p><p>These have been reported by many of you and the development team worked hard
        to fix them in the meantime. The result of this effort is the new
        KMyMoney 5.0.2 release.</p><p>Despite even more extensive testing than usual, we understand that some
          bugs may have slipped past our best efforts.  If you find one of them,
          please forgive us, and be sure to report it, either to the <a href="mailto:kmymoney-devel@kde.org">mailing list</a>
          or on <a href="https://bugs.kde.org/enter_bug.cgi?product=kmymoney%26format=guided">bugs.kde.org</a>.</p><p>From here, we will continue to fix reported bugs, and working to add
          many requested additions and enhancements, as well as further improving
          performance.</p><p>Many thanks go out to KDE's sysadmin team. Ben supported us with numerous hours
          to get daily builds onto the <a href="https://build.kde.org">KDE CI system</a>.
          This even includes MS-Windows installers.
          Please feel free to visit our overview page of the CI builds at
          <a href="https://kmymoney.org/build.php">https://kmymoney.org/build.php</a>.</p><p>Thanks also to those users who provided valuable support by sending in test files saved by earlier versions as
          far back as <a href="https://kmymoney.org/news.php#itemKMyMoney466released">KMyMoney 4.6.6</a>. Using these files
          we were able to fix some upgrade problems.</p><p>Here is the list of the bugs which have been fixed:
          <ul>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=283784">283784</a> when using the 'amount entry widget' in the ledger view, it hangs off the screen</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=340244">340244</a> Update the documentation screenshots</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=340902">340902</a> Saving anonymous file loses relation between investment accounts and brokerage accounts</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=343878">343878</a> investment account does not have key value pair for lastImportedTransactionDate</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=344409">344409</a> User is asked to create a new category in splits table if change is aborted</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=368190">368190</a> "Update Stock and Currency Prices" doesn't work when base currency is South Korean Won(KRW)</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=385180">385180</a> investactivities.cpp: 8 * Redundant condition</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390750">390750</a> Entering fees in Investment account (ie broker fee) doesn't appear with transaction</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=391251">391251</a> Double free or corruption when creating a new asset account.</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=392110">392110</a> Missing 2 .h files in /usr/include/kmymoney</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=392372">392372</a> 5.0.1 is shown as 5.0.0 in splash screen and about dialog</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=392407">392407</a> Home page Net Worth Forecast Not correct</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=392477">392477</a> Cannot create new investment</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=392519">392519</a> Reports Configure dialog uses a "Find" button instead of an "Apply" button.</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=392603">392603</a> OFX - Mapping Account - Last Update Change causing Update Account greying - Online Settings disappears</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=392735">392735</a> Crash on viewing or trying to add data into any account</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=393168">393168</a> [Patch] Fix working week in 5.x/master</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=393752">393752</a> Budgeted vs. Actual report: Budgeted values broken if ticks selected to Bi-Monthly, Quarterly or Yearly</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=394384">394384</a> MySQL connection error with dbname "KMyMoney"</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=394394">394394</a> problems adding securities to investment accounts</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=395025">395025</a> csv writer generates invalid file in case field delimiter is used in any field</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=395040">395040</a> CSV Export of Investment Accounts does not issue transactions on non-English kmymoney installations</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=395100">395100</a> Last selected view</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=395291">395291</a> KMyMoney gitt head master, crash on opening any account</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=395459">395459</a> Ledger Input: Category Field</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=395985">395985</a> QIF Export manually typing file path adds .qif after every character</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=396174">396174</a> Cannot create new investment</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=396405">396405</a> Last digit of date field is obscured by "details column" in ledger view</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=396759">396759</a> CSV: ordering of buttons when finishing wizard</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=396886">396886</a> Online Banking Behavior Change Since 4.8</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=396987">396987</a> Payee "Suggest a category" does not work</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=397020">397020</a> Opening dates in "all dates" plot</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=397021">397021</a> Net worth does not appear</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=397023">397023</a> Default color of imported transaction in the ledger</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=397025">397025</a> Kmymoney version in help menu, splash screen and apt are different</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=397436">397436</a> Net worth forecast graph in new file shows formatting problem</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=397457">397457</a> double clicking in ledger activates edit, but does not select transaction clicked on</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=397675">397675</a> cannot connect to kmysql database because of prepended slash</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=397824">397824</a> Limit in number of securities</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=398168">398168</a> &#233;cran d'accueil</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=398394">398394</a> Account information report crashes</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=398409">398409</a> KMyMoney does not prompt for password when connecting to database</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=399244">399244</a> KMyMoney does not work properly when changing from one file to another</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=399309">399309</a> "show balance chart" does not show 3 months forecast in the future anymore</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=399378">399378</a> Unable to Update Prices</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=399673">399673</a> importer file selector does not show QIF files and does not remember last import directory</li>
          </ul>
        </p><p>Here are some of the new improvements found in this release:
          <ul>
            <li><a href="https://bugs.kde.org/show_bug.cgi?id=396797">396797</a> Online web source "KMyMoney Currency" does not support price pairs without decimal</li>
            <li>Speedup loading of home page</li>
            <li>KDE binary factory builds for MS-Windows installer</li>
            <li>Add color coding of pos/neg amounts for 'posted value' column</li>
            <li>Fix online credit transfer creation</li>
            <li>Some features have been moved to plugins so that they can be turned off if unused</li>
            <li>Provide correct version information for all shared object files</li>
            <li>Improved handling of "Save as..." logic</li>
            <li>Allow to add a timezone offset for OFX import on account basis</li>
            <li>Moved GPG key handling to XML plugin</li>
            <li>Accounts can now take a URL for direct access of the online banking web-site</li>
            <li>Support more than one online banking provider during update</li>
            <li>Added option to keep linebreaks in memo of AqBanking transaction imports</li>
            <li>Remove reference to online balance when unmapping account</li>
            <li>Added feature to make DB password visible during input</li>
          </ul>
        </p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-5.0.2.txt">changelog</a>. We highly recommend upgrading to 5.0.2.</p><p>The KMyMoney Development Team</p>